library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity forwarding is
    port ( register_a     : in  std_logic_vector(3 downto 0);
           register_b     : in  std_logic_vector(3 downto 0);
           write_register : in  std_logic_vector(3 downto 0);
           reg_write      : in  std_logic;
           branch         : in  std_logic;

           forwarding_a         : out std_logic;
           forwarding_b         : out std_logic );
end forwarding;

architecture Behavioral of forwarding is

begin

  process ( register_a, 
			register_b, 
			write_register, 
			reg_write,
			branch )  
 
  begin  
    forwarding_a <= '0'; 
    forwarding_b <= '0'; 
    if (reg_write = '1') then 
      if (branch = '1') then
			if (write_register = "1111") then
				forwarding_b <= '1' after 0.2 ns;
			elsif (write_register = "1110") then
				forwarding_a <= '1' after 0.2 ns;
			end if;
      else
			if (write_register = register_a) then 
				forwarding_a <= '1' after 0.2 ns; 
			elsif (write_register = register_b) then 
				forwarding_b <= '1' after 0.2 ns; 
			end if; 
      end if;
    end if; 
     
  end process; 
 

end Behavioral;