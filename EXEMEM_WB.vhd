library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity exemem_wb is
    port ( reset               : in  std_logic;
           clk                 : in  std_logic;
		   
           regwrt_in           : in  std_logic;
           memtoreg_in         : in  std_logic;
           data_mem_in         : in  std_logic_vector(15 downto 0);
           alu_result_in       : in  std_logic_vector(15 downto 0);
           write_register_in   : in  std_logic_vector(3 downto 0);

           regwrt_out          : out std_logic;
           memtoreg_out        : out std_logic;
           data_mem_out        : out std_logic_vector(15 downto 0);
           alu_result_out      : out std_logic_vector(15 downto 0);
           write_register_out  : out std_logic_vector(3 downto 0);
		   stall			   : in  std_logic		   );
end exemem_wb;

architecture Behavioral of exemem_wb is

begin

    process ( reset,
                 clk,
                 regwrt_in,
                 memtoreg_in,
                 data_mem_in,
                 alu_result_in,
                 write_register_in ) is

     -- declare variables 
     variable regwrt          : std_logic;
     variable memtoreg        : std_logic;
     variable data_mem        : std_logic_vector(15 downto 0);
     variable alu_result      : std_logic_vector(15 downto 0);
     variable write_register  : std_logic_vector(3 downto 0);

    begin            
            if (reset = '1') then
                -- initial values of the registers - reset all registers to zeroes
                regwrt           := '0';
                memtoreg         := '0';
                data_mem         := X"0000";
                alu_result       := X"0000";
                write_register   := X"0";
            
            elsif (rising_edge(clk) and stall = '0') then
                -- register write on the falling clock edge
                regwrt          := regwrt_in;
                memtoreg        := memtoreg_in;
                data_mem        := data_mem_in;
                alu_result      := alu_result_in;
                write_register  := write_register_in;
            end if;
            
            regwrt_out          <= regwrt after 0.5 ns;
            memtoreg_out        <= memtoreg after 0.5 ns;
            data_mem_out        <= data_mem after 0.5 ns;
            alu_result_out      <= alu_result after 0.5 ns;
            write_register_out  <= write_register after 0.5 ns;
    end process; 

end Behavioral;

