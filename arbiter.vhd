---------------------------------------------------------------------------
-- data_memory.vhd - Implementation of A Single-Port, 16 x 16-bit Data
--                   Memory.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
library STD;
use STD.textio.all;  
use IEEE.std_logic_textio.all; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity arbiter is
    port ( apply1		: in  std_logic;
		   apply2		: in  std_logic;
		   sig_grant1	: out std_logic;
		   sig_grant2	: out std_logic);
end arbiter;

architecture behavioral of arbiter is
begin
	process(apply1,apply2)
	begin
		sig_grant1 <= '0';
		sig_grant2 <= '0';
		if (apply1 = '1') then
			sig_grant1 <= '1';
		elsif (apply1 = '0' and apply2 = '1') then
			sig_grant2<= '1';
		end if;
	end process;
end behavioral;
