library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity multi_processor is
    port ( reset  : in  std_logic;
           clk    : in  std_logic);
end multi_processor;
architecture structural of multi_processor is

COMPONENT single_cycle_core
	PORT (reset : in std_logic;
		clk : in std_logic;
		id  : in std_logic;
		sig_grant : in std_logic;
		sig_apply : out std_logic;
		wb		: out 	std_logic;
		rb		: out 	std_logic;
		ldb		: out 	std_logic;
		addrb	: out	std_logic_vector(15 downto 0);
		datawb	: out 	std_logic_vector(15 downto 0);
		datarb	: in 	std_logic_vector(15 downto 0)
	);
END COMPONENT;
COMPONENT arbiter
	PORT ( apply1		: in  std_logic;
		   apply2		: in  std_logic;
		   sig_grant1	: out std_logic;
		   sig_grant2	: out std_logic
	);
END COMPONENT;

COMPONENT shared_dm
	PORT ( reset        : in  std_logic;
           clk          : in  std_logic;
		   w1			: in  std_logic;
		   r1			: in  std_logic;
		   ld1			: in  std_logic;
		   write_data1   : in  std_logic_vector(15 downto 0);
           addr_in1      : in  std_logic_vector(15 downto 0);
           data_out1     : out std_logic_vector(15 downto 0);
		   
		   w2			: in  std_logic;
		   r2 			: in  std_logic;
		   ld2			: in  std_logic;
		   write_data2   : in  std_logic_vector(15 downto 0);
           addr_in2      : in  std_logic_vector(15 downto 0);
           data_out2     : out std_logic_vector(15 downto 0)
	);
END COMPONENT;

signal sig_apply : std_logic_vector(1 to 2);
signal sig_grant : std_logic_vector(1 to 2);

signal dm_w_1	 : std_logic;
signal dm_r_1	 : std_logic;
signal dm_ld_1	 : std_logic;
signal dm_in_1   : std_logic_vector(15 downto 0);
signal dm_addr_1 : std_logic_vector(15 downto 0);
signal dm_out_1  : std_logic_vector(15 downto 0);

signal dm_w_2	 : std_logic;
signal dm_r_2	 : std_logic;
signal dm_ld_2	 : std_logic;
signal dm_in_2   : std_logic_vector(15 downto 0);
signal dm_addr_2 : std_logic_vector(15 downto 0);
signal dm_out_2  : std_logic_vector(15 downto 0);

begin
	C1: single_cycle_core
		port map (reset => reset,
			clk => clk,
			id => '0',
			sig_grant => sig_grant(1),
			sig_apply => sig_apply(1),
			wb		=> dm_w_1,
			rb		=> dm_r_1,
			ldb		=> dm_ld_1,
			addrb	=> dm_addr_1,
			datawb	=> dm_in_1,
			datarb	=> dm_out_1
		);
	C2: single_cycle_core
		port map (reset => reset,
			clk => clk,
			id => '1',
			sig_grant => sig_grant(2),
			sig_apply => sig_apply(2),
			wb		=> dm_w_2,
			rb		=> dm_r_2,
			ldb		=> dm_ld_2,
			addrb	=> dm_addr_2,
			datawb	=> dm_in_2,
			datarb	=> dm_out_2
		);
	AB : arbiter
		port map(
		apply1		=> sig_apply(1),
		apply2		=> sig_apply(2),
		sig_grant1	=> sig_grant(1),
		sig_grant2	=> sig_grant(2)
	);
	
	
	SDM: shared_dm
	port map( reset 	=> reset,
           clk			=> clk,
		   w1			=> dm_w_1,
		   r1			=> dm_r_1,
		   ld1			=> dm_ld_1,
		   write_data1   => dm_in_1,
           addr_in1      => dm_addr_1,
           data_out1     => dm_out_1,
		   
		   w2			=> dm_w_2,
		   r2 			=> dm_r_2,
		   ld2			=> dm_ld_2,
		   write_data2   => dm_in_2,
           addr_in2      => dm_addr_2,
           data_out2     => dm_out_2
	);
end structural;