---------------------------------------------------------------------------
-- control_unit.vhd - Control Unit Implementation
--
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
--  control signals:
--     reg_dst    : asserted for ADD instructions, so that the register
--                  destination number for the 'write_register' comes from
--                  the rd field (bits 3-0). 
--     reg_write  : asserted for ADD and LOAD instructions, so that the
--                  register on the 'write_register' input is written with
--                  the value on the 'write_data' port.
--     alu_src    : asserted for LOAD and STORE instructions, so that the
--                  second ALU operand is the sign-extended, lower 4 bits
--                  of the instruction.
--     mem_write  : asserted for STORE instructions, so that the data 
--                  memory contents designated by the address input are
--                  replaced by the value on the 'write_data' input.
--     mem_to_reg : asserted for LOAD instructions, so that the value fed
--                  to the register 'write_data' input comes from the
--                  data memory.
--
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

--custom signal
--branch
--aluop

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity control_unit is
    port ( opcode     : in  std_logic_vector(3 downto 0);
           reg_dst    : out std_logic;
           reg_write  : out std_logic;
           alu_src    : out std_logic;
           mem_write  : out std_logic;
           mem_to_reg : out std_logic;
		   alu_op 	  : out std_logic_vector(0 to 3);
		   branch 	  : out std_logic;
		   mem_loadfile:out std_logic);
end control_unit;

architecture behavioural of control_unit is


--opcode
constant OP_LOAD  : std_logic_vector(3 downto 0) := "0001"; -- 1
constant OP_STORE : std_logic_vector(3 downto 0) := "0011"; -- 3
constant OP_ADD   : std_logic_vector(3 downto 0) := "1000"; -- 8

constant OP_ADDI  : std_logic_vector(3 downto 0) := "1001"; -- 9
constant OP_SUB	  : std_logic_vector(3 downto 0) := "1010"; -- 10
constant OP_LDFLE : std_logic_vector(3 downto 0) := "1011"; -- 11
constant OP_BGE   : std_logic_vector(3 downto 0) := "0100"; -- 4
constant OP_BEQ   : std_logic_vector(3 downto 0) := "0101"; -- 5
constant OP_BNE   : std_logic_vector(3 downto 0) := "0110"; -- 6
constant OP_BLT   : std_logic_vector(3 downto 0) := "0111"; -- 7

constant OP_J     : std_logic_vector(3 downto 0) := "1111"; -- 15

-- ALU-OP
constant ALU_ADD : std_logic_vector(0 to 3) := "0000"; -- 0
constant ALU_SUB : std_logic_vector(0 to 3) := "0001"; -- 1
constant ALU_EQU : std_logic_vector(0 to 3) := "0100"; -- 4
constant ALU_GEQ : std_logic_vector(0 to 3) := "0101"; -- 5
constant ALU_J   : std_logic_vector(0 to 3) := "0110"; -- 6
constant ALU_DEF : std_logic_vector(0 to 3) := "0111"; -- 7
constant ALU_LES : std_logic_vector(0 to 3) := "1000"; -- 8
constant ALU_NEQ : std_logic_vector(0 to 3) := "1001"; -- 9

begin

    reg_dst    <= '1' when (opcode = OP_ADD or 
							opcode = OP_SUB ) else
                  '0';

    reg_write  <= '1' when (opcode = OP_ADD or
                            opcode = OP_LOAD or
							opcode = OP_ADDI or
							opcode = OP_SUB  ) else
                  '0';
    
    alu_src    <= '1' when (opcode = OP_LOAD or
							opcode = OP_STORE or
							opcode = OP_ADDI ) else
                  '0';
                 
    mem_write  <= '1' when opcode = OP_STORE else
                  '0';
             
    mem_to_reg <= '1' when opcode = OP_LOAD else
                  '0';
	
	mem_loadfile <= '1' when opcode = OP_LDFLE else
					'0';
					
	branch <= '1' when (opcode = OP_BEQ or
						opcode = OP_BGE or
						opcode = OP_BNE or
						opcode = OP_BLT or
						opcode = OP_J) else
			  '0';
			  
	alu_op <= ALU_ADD when (opcode = OP_ADD or opcode = OP_ADDI) else
			  ALU_SUB when (opcode = OP_SUB) else
			  ALU_EQU when (opcode = OP_BEQ) else
			  ALU_LES when (opcode = OP_BLT) else
			  ALU_NEQ when (opcode = OP_BNE) else
			  ALU_GEQ when (opcode = OP_BGE) else
			  ALU_J   when (opcode = OP_J) else
			  ALU_DEF;	
end behavioural;
