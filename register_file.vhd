----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:31:50 04/06/2016 
-- Design Name: 
-- Module Name:    register_file - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity register_file is
	port ( reset           	: in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0);
		   stall		   : in  std_logic);
end register_file;

architecture Behavioral of register_file is

-- reg_file is an array of 15 elements/registers. 
-- Each registers is of 16 bit. 
type reg_file is array(0 to 15) of std_logic_vector(15 downto 0);
signal sig_regfile : reg_file;

begin

	process ( reset,
				 clk,
				 read_register_a,
				 read_register_b,
				 write_enable,
				 write_register,
				 write_data ) is

	 -- declare variables 
	 variable var_regfile     : reg_file;
	 variable var_read_addr_a : integer;
	 variable var_read_addr_b : integer;
	 variable var_write_addr  : integer;
    
    begin
			--if (stall = '0') then
				var_read_addr_a := conv_integer(read_register_a);
				var_read_addr_b := conv_integer(read_register_b);
				var_write_addr  := conv_integer(write_register);
				
				if (reset = '1') then
					-- initial values of the registers - reset all registers to zeroes
					var_regfile := (others => X"0000");
					var_regfile(3) :=X"0040";
					var_regfile(13) :=X"0080";
					
				elsif (falling_edge(clk) and write_enable = '1') then
					-- register write on the falling clock edge
					var_regfile(var_write_addr) := write_data;
				end if;
				
				-- enforces value zero for register $0
				var_regfile(0) := X"0000";
				
				
				-- continuous read of the registers at location 
				-- read_register_a and read_register_b
				read_data_a <= var_regfile(var_read_addr_a) after 2 ns; 
				read_data_b <= var_regfile(var_read_addr_b) after 2 ns;
				
				-- the following are probe signals (for simulation purpose)
				sig_regfile <= var_regfile;
			--end if;
    end process; 

end Behavioral;

