---------------------------------------------------------------------------
-- data_memory.vhd - Implementation of A Single-Port, 16 x 16-bit Data
--                   Memory.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
library STD;
use STD.textio.all;  
use IEEE.std_logic_textio.all; 
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
		   mem_loadfile : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(15 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end data_memory;

architecture behavioral of data_memory is
constant SIZE : integer := 255;
type mem_array is array(0 to SIZE) of std_logic_vector(15 downto 0);
signal sig_data_mem : mem_array;



begin

    mem_process: process ( clk,
                           write_enable,
                           write_data,
                           addr_in ) is
  
    variable var_data_mem : mem_array;
    variable var_addr     : integer;
	variable i : integer := 0; 
	variable n : integer := 65;

	file patternfile : text open read_mode is "pattern.txt";
	variable inline : line;
	variable char : character:='0';

	file stringfile : text open read_mode is "string1.txt";
	variable stringline : line;
	variable str : character:='0';
	variable isread : boolean;
	
	file stringfile2 : text open read_mode is "string2.txt";
	variable stringline2 : line;
	variable str2 : character:='0';
	variable isread2 : boolean;
    begin
        var_addr := conv_integer(addr_in(7 downto 0));
        
        if (reset = '1') then
			if not endfile(patternfile) then
				readline(patternfile,inline);
				for j in inline'range loop
					READ(inline, char);
					var_data_mem(i) := std_logic_vector(to_unsigned(character'pos(char),16));
					i := i + 1;
				end loop;
			end if;
			
			for j in i to 63 loop
				var_data_mem(j) := X"0000";
			end loop;
			
			-- pattern length always store in Mem[64]
			var_data_mem(64) := X"00" & std_logic_vector(to_unsigned(i,8));
		
            -- initial values of the data memory : reset to zero
			if not endfile(stringfile) then
				readline(stringfile,stringline);
				for j in 0 to i-1 loop
					READ(stringline, char);
					var_data_mem(n) := std_logic_vector(to_unsigned(character'pos(char),16));
					n := n + 1;
				end loop;
			end if;
			
			for j in n to 255 loop
				var_data_mem(j) := X"0000";
			end loop;
			
			
			
			
			
        elsif (falling_edge(clk) and write_enable = '1') then
            -- memory writes on the falling clock edge
            var_data_mem(var_addr) := write_data;
			
        elsif (falling_edge(clk) and mem_loadfile = '1') then
			READ(stringline,str,isread);
			if isread then
				var_data_mem(var_addr) := std_logic_vector(to_unsigned(character'pos(str),16));
			else
				var_data_mem(var_addr) := X"0000";
			end if;
		end if;
		
	
		
		-- continuous read of the memory location given by var_addr 
        data_out <= var_data_mem(var_addr);
 
        -- the following are probe signals (for simulation purpose) 
        sig_data_mem <= var_data_mem after 3 ns;

    end process;
  
end behavioral;
