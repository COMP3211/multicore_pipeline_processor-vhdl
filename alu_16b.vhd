
---------------------------------------------------------------------------
-- alu_16b.vhd - 16-bit Adder Implementation
--
--
-- Copyright (C) 2017 by Weinan Wang (weinan.wang@student.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.NUMERIC_STD.ALL;

entity alu_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
		   alu_op    : in  std_logic_vector(0 to 3);
           result    : out std_logic_vector(15 downto 0);
		   zerotag   : out std_logic;
           carry_out : out std_logic );
end alu_16b;

architecture behavioural of alu_16b is

signal sig_result : std_logic_vector(16 downto 0);
constant ALU_ADD : std_logic_vector(0 to 3) := "0000"; -- 0
constant ALU_SUB : std_logic_vector(0 to 3) := "0001"; -- 1
constant ALU_EQU : std_logic_vector(0 to 3) := "0100"; -- 4
constant ALU_GEQ : std_logic_vector(0 to 3) := "0101"; -- 5
constant ALU_J   : std_logic_vector(0 to 3) := "0110"; -- 6
constant ALU_DEF : std_logic_vector(0 to 3) := "0111"; -- 7
constant ALU_LES : std_logic_vector(0 to 3) := "1000"; -- 8
constant ALU_NEQ : std_logic_vector(0 to 3) := "1001"; -- 9

begin

	alu_process:process (src_a,src_b,alu_op) is
	begin
		case alu_op is
			when ALU_ADD => sig_result <= ('0' & src_a ) + ('0' & src_b);
			when ALU_SUB => sig_result <= ('0' & src_a ) - ('0' & src_b);
			when others => sig_result <= ('0' & src_a ) + ('0' & src_b);
		end case;
		
	end process;
    result     <= sig_result(15 downto 0);
    carry_out  <= sig_result(16);
	
	-- zerotag means it has to branch
    zerotag    <= '1' when (((src_a = src_b) and (alu_op = ALU_EQU)) or 
							((src_a >= src_b) and (alu_op = ALU_GEQ)) or
							((alu_op = ALU_LES) and (src_a < src_b)) or 
							((alu_op = ALU_NEQ) and (src_a /= src_b)) or
							(alu_op = ALU_J))
 else
	               '0';
end behavioural;
