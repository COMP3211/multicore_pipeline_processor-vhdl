library IEEE;
library STD;
use STD.textio.all;  
use IEEE.std_logic_textio.all; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity shared_dm is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
		   w1			: in  std_logic;
		   r1			: in  std_logic;
		   ld1			: in  std_logic;
		   write_data1   : in  std_logic_vector(15 downto 0);
           addr_in1      : in  std_logic_vector(15 downto 0);
           data_out1     : out std_logic_vector(15 downto 0);
		   
		   w2			: in  std_logic;
		   r2 			: in  std_logic;
		   ld2			: in  std_logic;
		   write_data2   : in  std_logic_vector(15 downto 0);
           addr_in2      : in  std_logic_vector(15 downto 0);
           data_out2     : out std_logic_vector(15 downto 0));
end shared_dm;
architecture behavioral of shared_dm is
constant SIZE : integer := 255;
type mem_array is array(0 to SIZE) of std_logic_vector(15 downto 0);
signal sig_data_mem : mem_array;

begin

    mem_process: process (reset,w1,r1,ld1,addr_in1,
							w2,r2,ld2,addr_in2) is
  
    variable var_data_mem : mem_array;
    variable var_addr     : integer;
	variable i : integer := 0; 
	variable n : integer := 65;
	variable m : integer := 129;
	file patternfile : text open read_mode is "pattern.txt";
	variable inline : line;
	variable char : character:='0';

	file stringfile1 : text open read_mode is "string1.txt";
	variable stringline1 : line;
	variable str1 : character:='0';
	variable isread1 : boolean;
	
	file stringfile2 : text open read_mode is "string2.txt";
	variable stringline2 : line;
	variable str2 : character:='0';
	variable isread2 : boolean;
    begin
    
        if (reset = '1') then
			if not endfile(patternfile) then
				readline(patternfile,inline);
				for j in inline'range loop
					READ(inline, char);
					var_data_mem(i) := std_logic_vector(to_unsigned(character'pos(char),16));
					i := i + 1;
				end loop;
			end if;		

			for j in i to 63 loop
				var_data_mem(j) := X"0000";
			end loop;
			
			-- -- pattern length always store in Mem[64]
			var_data_mem(64) := X"00" & std_logic_vector(to_unsigned(i,8));
		
            -- initial values of the data memory : reset to zero
			if not endfile(stringfile1) then
				readline(stringfile1,stringline1);
				for j in 0 to i-1 loop
					READ(stringline1, str1);
					var_data_mem(n) := std_logic_vector(to_unsigned(character'pos(str1),16));
					n := n + 1;
				end loop;
			end if;
			
			for j in n to 128 loop
				var_data_mem(j) := X"0000";
			end loop;
			
			if not endfile(stringfile2) then
				readline(stringfile2,stringline2);
				for j in 0 to i-1 loop
					READ(stringline2, str2);
					var_data_mem(m) := std_logic_vector(to_unsigned(character'pos(str2),16));
					m := m + 1;
				end loop;
			end if;
			
			for j in m to 255 loop
				var_data_mem(j) := X"0000";
			end loop;
			
		else
			var_addr := conv_integer(addr_in1(7 downto 0));
			if ( w1	= '1') then
				var_data_mem(var_addr) := write_data1;
			elsif ( r1 = '1') then
				data_out1 <= var_data_mem(var_addr);
			elsif ( ld1	= '1') then
				READ(stringline1,str1,isread1);
				if isread1 then
					var_data_mem(var_addr) := std_logic_vector(to_unsigned(character'pos(str1),16));
				else
					var_data_mem(var_addr) := X"0000";
				end if;
			end if;
			
			var_addr := conv_integer(addr_in2(7 downto 0));
			if ( w2	= '1') then
				var_data_mem(var_addr) := write_data2;
			elsif ( r2 = '1') then
				data_out2 <= var_data_mem(var_addr);
			elsif ( ld2	= '1') then
				READ(stringline2,str2,isread2);
				if isread2 then
					var_data_mem(var_addr) := std_logic_vector(to_unsigned(character'pos(str2),16));
				else
					var_data_mem(var_addr) := X"0000";
				end if;
			end if;
		end if;
		var_data_mem(253) := var_data_mem(254) + var_data_mem(255);
        sig_data_mem <= var_data_mem after 3 ns;
    end process;
  
end behavioral;
